#!/bin/bash

# Archive dir
ARCHIVE_DIR="${HOME}/eos_atlaspmb/archive/custom"

# Web dir
# The actual EOS path doesn't work w/ mkdir -p
# Therefore, I'm using a soft-link from ${HOME} instead.
# See JIRA EOS-4364
WEB_DIR="${HOME}/www_atlaspmb"

# Execute the specific job
#hi jobs
#upc stream
run_hi_upc(){

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
          --CA 'True' \
          --maxEvents ${NEVENTS} \
          --perfmon 'fullmonmt' \
          --multithreaded 'True' \
          --conditionsTag="CONDBR2-BLKPA-2022-09" \
          --geometryVersion="ATLAS-R3S-2021-03-01-00" \
          --preExec="flags.Reco.HIMode=HIMode.UPC" \
          --postExec 'all:cfg.getService("AlgResourcePool").CountAlgorithmInstanceMisses = True' \
          --inputBSFile "/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/HI/UPC/data23_hi.00463427.physics_UPC.daq.RAW._lb0585._SFO-17._0001.data" \
          --outputAODFile=myAOD.pool.root >  __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;
}

#upc stream
run_hi_hp(){

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
          --CA 'True' \
          --maxEvents ${NEVENTS} \
          --perfmon 'fullmonmt' \
          --multithreaded 'True' \
          --conditionsTag="CONDBR2-BLKPA-2022-09" \
          --geometryVersion="ATLAS-R3S-2021-03-01-00" \
          --preExec="flags.Reco.HIMode=HIMode.HI" \
          --postExec 'all:cfg.getService("AlgResourcePool").CountAlgorithmInstanceMisses = True' \
          --inputBSFile "/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/HI/data23_hi.00463427.physics_HardProbes.daq.RAW._lb0585._SFO-11._0001.data" \
          --outputAODFile 'myAOD.pool.root' >  __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;
}

# Phase2 - FullSimulation
run_phase2_fullsim_mt8() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Sim_tf.py \
      --CA 'True' \
      --multithreaded 'True' \
      --maxEvents ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --conditionsTag 'default:OFLCOND-MC15c-SDR-14-05' \
      --geometryVersion 'default:ATLAS-P2-RUN4-03-00-00' \
      --postInclude 'default:PyJobTransforms.UseFrontier' \
      --preInclude 'EVNTtoHITS:Campaigns.PhaseIISimulation' \
      --postExec 'all:cfg.getService("AlgResourcePool").CountAlgorithmInstanceMisses = True' \
      --simulator 'FullG4MT' \
      --inputEVNTFile '/eos/atlas/atlascerngroupdisk/data-art/grid-input/PhaseIIUpgrade/EVNT/mc21_14TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.evgen.EVNT.e8481/*' \
      --outputHITSFile 'myHITS.pool.root' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# Phase 2 - RecoOnly Fast Tracking
run_phase2_recoonly_fasttracking() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    DATADIR="/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/PhaseIIUpgrade/RDO"

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
          --CA 'all:True' \
          --maxEvents ${NEVENTS} \
          --perfmon 'fullmonmt' \
          --multithreaded 'True' \
          --autoConfiguration 'everything' \
          --conditionsTag 'all:OFLCOND-MC15c-SDR-14-05' \
          --geometryVersion 'all:ATLAS-P2-RUN4-03-00-00' \
          --postInclude 'all:PyJobTransforms.UseFrontier' \
          --preInclude 'all:Campaigns.PhaseIIPileUp200' \
          --steering 'doRAWtoALL' \
          --preExec 'all:flags.Tracking.doITkFastTracking=True' \
          --postExec 'all:cfg.getService("AlgResourcePool").CountAlgorithmInstanceMisses = True' \
          --inputRDOFile "${DATADIR}/ATLAS-P2-RUN4-03-00-00/mc21_14TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.RDO.e8481_s4149_r14700/*" \
          --outputAODFile 'myAOD.pool.root' >  __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# Phase 2 - RecoOnly Tracking
run_phase2_recoonly_tracking() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    DATADIR="/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/PhaseIIUpgrade/RDO"
    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} Reco_tf.py \
      --CA 'all:True' \
      --maxEvents ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --multithreaded 'True' \
      --autoConfiguration 'everything' \
      --conditionsTag 'all:OFLCOND-MC15c-SDR-14-05' \
      --geometryVersion 'all:ATLAS-P2-RUN4-03-00-00' \
      --postInclude 'all:PyJobTransforms.UseFrontier' \
      --preInclude 'all:Campaigns.PhaseIIPileUp200' \
      --steering 'doRAWtoALL' \
      --preExec 'all:ConfigFlags.Tracking.doITkFastTracking=False' \
      --postExec 'all:cfg.getService("AlgResourcePool").CountAlgorithmInstanceMisses = True' \
      --inputRDOFile "${DATADIR}/ATLAS-P2-RUN4-03-00-00/mc21_14TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.RDO.e8481_s4149_r14700/*" \
      --outputAODFile 'myAOD.pool.root' \
      --jobNumber '1' >  __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# Phase 2 - RecoOnly Tracking
run_phase2_recoonly_actstracking() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    DATADIR="/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/PhaseIIUpgrade/RDO"
    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} Reco_tf.py \
      --CA 'all:True' \
      --maxEvents  ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --multithreaded 'True' \
      --autoConfiguration 'everything' \
      --conditionsTag 'all:OFLCOND-MC15c-SDR-14-05' \
      --geometryVersion 'all:ATLAS-P2-RUN4-03-00-00' \
      --postInclude 'all:PyJobTransforms.UseFrontier' \
      --preInclude "InDetConfig.ConfigurationHelpers.OnlyTrackingPreInclude,ActsConfig.ActsCIFlags.actsBenchmarkSpotFlags" \
      --steering 'doRAWtoALL' \
      --preExec 'all:ConfigFlags.Tracking.doITkFastTracking=False' \
      --postExec "all:cfg.getService('AlgResourcePool').CountAlgorithmInstanceMisses = True;cfg.getEventAlgo('ActsTrkITkPixelClusterizationAlg').PixelClustersKey='xAODpixelClusters';cfg.getEventAlgo('ActsTrkITkStripClusterizationAlg').StripClustersKey='xAODstripClusters';" \
      --inputRDOFile "${DATADIR}/ATLAS-P2-RUN4-03-00-00/mc21_14TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.RDO.e8481_s4149_r14700/*" \
      --outputAODFile 'myAOD.pool.root' \
      --jobNumber '1' >  __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}


# mc21a DAOD
run_mc21a_daod() {

    # Parse the inputs
    FORMAT=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    Derivation_tf.py \
      --CA 'True' \
      --maxEvents ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --inputAODFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/AODtoDAOD/mc21a/myAOD.pool.root' \
      --outputDAODFile 'pool.root' \
      --formats ${FORMAT} > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# data22 DAOD
run_data22_daod() {

    # Parse the inputs
    FORMAT=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    Derivation_tf.py \
      --CA 'True' \
      --maxEvents ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --inputAODFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/AODtoDAOD/data22/myAOD.pool.root' \
      --outputDAODFile 'pool.root' \
      --formats ${FORMAT} > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# data22 DAOD
run_data23_daod() {

    # Parse the inputs
    FORMAT=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    Derivation_tf.py \
      --CA 'True' \
      --maxEvents ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --inputAODFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/AODtoDAOD/data23/myAOD.pool.root' \
      --outputDAODFile 'pool.root' \
      --formats ${FORMAT} > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

run_data23_daod_ntp() {

    # Parse the inputs
    FORMAT=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    Derivation_tf.py \
      --CA 'True' \
      --maxEvents ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --inputAODFile="/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/AODtoDAOD/data23/myAOD.pool.root" \
      --outputDAODFile 'pool.root' \
      --preExec="flags.Output.StorageTechnology.EventData=\"ROOTRNTUPLE\";" \
      --formats ${FORMAT} > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;
}


# mc21a DAOD
run_mc21a_daod_ntp() {

    # Parse the inputs
    FORMAT=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    Derivation_tf.py \
      --CA 'True' \
      --maxEvents ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --inputAODFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/mc21a/myAOD.pool.root' \
      --preExec="flags.Output.StorageTechnology.EventData=\"ROOTRNTUPLE\";" \
      --outputDAODFile 'pool.root' \
      --formats ${FORMAT} > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}
# mc21a full-chain MC
run_mc21a() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    DATADIR="/data/atlaspmb/spot-job-inputs/mc21_13p6TeV"

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --CA 'default:True' 'RDOtoRDOTrigger:False' \
      --inputHITSFile "${DATADIR}/mc21_13p6TeV.601237.PhPy8EG_A14_ttbar_hdamp258p75_allhad.merge.HITS.e8453_e8455_s3873_s3874/HITS.29625936._002926.pool.root.1" \
      --inputRDO_BKGFile "${DATADIR}/mc21_13p6TeV.900149.PG_single_nu_Pt50.merge.RDO.e8453_e8455_s3864_d1761_d1758/RDO.29616553._005014.pool.root.1" \
      --outputAODFile 'myAOD.pool.root' \
      --perfmon 'fullmonmt' \
      --maxEvents ${NEVENTS} \
      --multithreaded='True' \
      --preInclude 'all:Campaigns/MC21a.py' \
      --postInclude 'default:PyJobTransforms/UseFrontier.py' \
      --skipEvents '0' \
      --autoConfiguration 'everything' \
      --conditionsTag 'default:OFLCOND-MC21-SDR-RUN3-07' \
      --geometryVersion 'default:ATLAS-R3S-2021-03-00-00' \
      --runNumber='601237' \
      --digiSeedOffset1 '232' \
      --digiSeedOffset2 '232' \
      --AMITag='r13768' \
      --steering 'doOverlay' 'doRDO_TRIG' 'doTRIGtoALL' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# mc23c full-chain MC
run_mc23c() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    DATADIR="/data/atlaspmb/spot-job-inputs/mc23_13p6TeV"

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --CA 'default:True' \
      --inputHITSFile "${DATADIR}/HITS/mc23_13p6TeV.601237.PhPy8EG_A14_ttbar_hdamp258p75_allhad.merge.HITS.e8514_e8528_s4159_s4114/HITS.34124871._003416.pool.root.1" \
      --inputRDO_BKGFile "${DATADIR}/RDO/mc23_13p6TeV.900149.PG_single_nu_Pt50.merge.RDO.e8514_e8528_s4153_d1879_d1880/RDO.33837536._002942.pool.root.1" \
      --outputAODFile 'myAOD.pool.root' \
      --perfmon 'fullmonmt' \
      --maxEvents ${NEVENTS} \
      --multithreaded='True' \
      --preInclude 'all:Campaigns.MC23c' \
      --postInclude 'default:PyJobTransforms.UseFrontier' \
      --skipEvents '0' \
      --autoConfiguration 'everything' \
      --conditionsTag 'default:OFLCOND-MC23-SDR-RUN3-05' \
      --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' \
      --runNumber '601237' \
      --digiSeedOffset1 '232' \
      --digiSeedOffset2 '232' \
      --AMITag 'r14799' \
      --steering 'doOverlay' 'doRDO_TRIG' 'doTRIGtoALL' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# q445 based full-chain MC
run_q445() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --AMI 'q445' \
      --CA 'default:True' \
      --perfmon 'fullmonmt' \
      --maxEvents ${NEVENTS} \
      --outputAODFile 'myAOD.pool.root' \
      --multithreaded 'True' \
      --deleteIntermediateOutputfiles 'True' \
      --steering 'doRDO_TRIG' 'doTRIGtoALL' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# RDOtoRDOTrigger from q221
run_r2rt() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --inputRDOFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TriggerTest/valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.RDO.e4993_s3214_r11315/RDO.17533168._000001.pool.root.1' \
      --AMI 'q221' \
      --perfmon 'fullmonmt' \
      --conditionsTag 'OFLCOND-MC16-SDR-RUN2-09' \
      --maxEvents ${NEVENTS} \
      --outputRDO_TRIGFile 'myRDOTrigger.pool.root' \
      --multithreaded 'True' \
      --preExec 'RDOtoRDOTrigger:' \
      --postExec 'RDOtoRDOTrigger:svcMgr.AlgResourcePool.CountAlgorithmInstanceMisses=True;' \
      --steering 'doRDO_TRIG' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}
# RAWtoALL + DQ
run_r2a_data23() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    MINFIT=${3}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --CA  'True' \
      --perfmon 'fullmonmt' \
      --inputBSFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/data23_13p6TeV/data23_13p6TeV.00451569.physics_Main.daq.RAW._lb0260._SFO-14._0001.data' \
      --maxEvents ${NEVENTS} \
      --outputAODFile 'myAOD.pool.root' \
      --outputHISTFile 'myHIST.root' \
      --multithreaded 'True' \
      --postExec "all:cfg.getService('PerfMonMTSvc').memFitLowerLimit = "${MINFIT}";cfg.getService('AlgResourcePool').CountAlgorithmInstanceMisses = True;" \
      --autoConfiguration 'everything' \
      --conditionsTag 'CONDBR2-BLKPA-2023-01' \
      --geometryVersion 'ATLAS-R3S-2021-03-02-00' \
      --runNumber '451569' \
      --steering 'doRAWtoALL' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}



# RAWtoALL + DQ
run_r3a_data23() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    MINFIT=${3}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --CA  'True' \
      --perfmon 'fullmonmt' \
      --inputBSFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/data23_13p6TeV/data23_13p6TeV.00454083.physics_Main.daq.RAW._lb1100._SFO-11._0001.data' \
      --maxEvents ${NEVENTS} \
      --outputAODFile 'myAOD.pool.root' \
      --outputHISTFile 'myHIST.root' \
      --multithreaded 'True' \
      --postExec "all:cfg.getService('PerfMonMTSvc').memFitLowerLimit = "${MINFIT}";cfg.getService('AlgResourcePool').CountAlgorithmInstanceMisses = True;" \
      --autoConfiguration 'everything' \
      --conditionsTag 'CONDBR2-BLKPA-2023-02' \
      --geometryVersion 'ATLAS-R3S-2021-03-02-00' \
      --runNumber '454083' \
      --steering 'doRAWtoALL' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}
# RAWtoALL + DQ
run_r2a_data22() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    MINFIT=${3}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --CA  'True' \
      --perfmon 'fullmonmt' \
      --inputBSFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/data22_13p6TeV/data22_13p6TeV.00437548.physics_Main.daq.RAW._lb1666._SFO-19._0001.data' \
      --maxEvents ${NEVENTS} \
      --outputAODFile 'myAOD.pool.root' \
      --outputHISTFile 'myHIST.root' \
      --multithreaded 'True' \
      --preExec 'all:ConfigFlags.Trigger.triggerConfig="DB";' \
      --postExec "all:cfg.getService('PerfMonMTSvc').memFitLowerLimit = "${MINFIT}";cfg.getService('AlgResourcePool').CountAlgorithmInstanceMisses = True;" \
      --autoConfiguration 'everything' \
      --conditionsTag 'CONDBR2-BLKPA-2022-08' \
      --geometryVersion 'ATLAS-R3S-2021-03-01-00' \
      --runNumber '437548' \
      --steering 'doRAWtoALL' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}



run_r2a_data18() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    MINFIT=${3}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --CA 'True' \
      --perfmon 'fullmonmt' \
      --inputBSFile '/data/atlaspmb/spot-job-inputs/data18_13TeV/data18_13TeV.00357750.physics_Main.daq.RAW._lb0105._SFO-4._0002.data' \
      --maxEvents ${NEVENTS} \
      --outputAODFile 'myAOD.pool.root' \
      --outputHISTFile 'myHIST.pool.root' \
      --multithreaded 'True' \
      --postExec "all:cfg.getService('PerfMonMTSvc').memFitLowerLimit = "${MINFIT}";cfg.getService('AlgResourcePool').CountAlgorithmInstanceMisses = True;" \
      --autoConfiguration 'everything' \
      --conditionsTag 'all:CONDBR2-BLKPA-RUN2-09' \
      --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
      --runNumber '357750' \
      --steering 'doRAWtoALL' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# ITk simulation
run_e2h_itk() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} Sim_tf.py \
      --CA 'True' \
      --inputEVNTFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/inputs/EVNT.01485091._001049.pool.root.1' \
      --maxEvents ${NEVENTS} \
      --outputHITSFile 'myHITS.pool.root' \
      --multithreaded 'True' \
      --perfmon 'fullmonmt' \
      --conditionsTag 'default:OFLCOND-MC15c-SDR-14-05' \
      --physicsList 'FTFP_BERT_ATL' \
      --truthStrategy 'MC15aPlus' \
      --simulator 'FullG4MT' \
      --postInclude 'default:PyJobTransforms.UseFrontier' \
      --preInclude 'EVNTtoHITS:SimuJobTransforms.BeamPipeKill,SimuJobTransforms.FrozenShowersFCalOnly,SimuJobTransforms.TightMuonStepping' \
      --postExec 'all:cfg.getService("AlgResourcePool").CountAlgorithmInstanceMisses = True' \
      --DataRunNumber '242000' \
      --geometryVersion 'default:ATLAS-P2-ITK-24-00-00' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# ttbar full-chain MC
run_mcttbar(){
    
    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    
    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} Sim_tf.py \
      --perfmon 'fullmonmt' \
      --CA 'True'\
      --multithreaded 'True' \
      --inputEVNTFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CampaignInputs/mc23/EVNT/mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.evgen.EVNT.e8514/EVNT.32288062._002040.pool.root.1' \
      --conditionsTag 'default:OFLCOND-MC21-SDR-RUN3-07' \
      --geometryVersion 'default:ATLAS-R3S-2021-03-02-00'   \
      --postExec 'all:cfg.getService("AlgResourcePool").CountAlgorithmInstanceMisses = True' \
      --AMIConfig 's4006' \
      --outputHITSFile 'myHITS.pool.root' \
      --maxEvents ${NEVENTS} \
      --jobNumber '1'  > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# ttbar fast MC
run_mcttbar_fast(){

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} Sim_tf.py \
      --perfmon 'fullmonmt' \
      --CA 'True'\
      --multithreaded 'True'\
      --conditionsTag 'default:OFLCOND-MC21-SDR-RUN3-07' \
      --simulator 'ATLFAST3MT_QS' \
      --postInclude 'PyJobTransforms.UseFrontier' \
      --preInclude 'EVNTtoHITS:Campaigns.MC23aSimulationMultipleIoV' \
      --preExec "EVNTtoHITS:flags.Sim.FastCalo.ParamsInputFilename=\"FastCaloSim/MC23/TFCSparam_dev_Hybrid_D_v1_all_baryons_0_500.root\";" \
      --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' \
      --postExec 'all:cfg.getService("AlgResourcePool").CountAlgorithmInstanceMisses = True' \
      --inputEVNTFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CampaignInputs/mc23/EVNT/mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.evgen.EVNT.e8514/EVNT.32288062._002040.pool.root.1' \
      --outputHITSFile 'myHITS.pool.root' \
      --maxEvents ${NEVENTS} \
      --jobNumber '1' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# ttbar digitization MC
run_digi_ttbar(){

    # Parse the inputs
    NEVENTS=${1}

    # Run the job
    export TRF_ECHO=1;
    Digi_tf.py \
       --perfmon 'fullmonmt' \
       --CA 'True'\
       --PileUpPresampling 'True' \
       --conditionsTag 'default:OFLCOND-MC21-SDR-RUN3-07' \
       --digiSeedOffset1 '170' \
       --digiSeedOffset2 '170' \
       --digiSteeringConf 'StandardSignalOnlyTruth' \
       --geometryVersion 'default:ATLAS-R3S-2021-03-00-00' \
       --inputHITSFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CampaignInputs/mc21/HITS/mc21_13p6TeV.900149.PG_single_nu_Pt50.simul.HITS.e8453_s3864/HITS.29241942._001453.pool.root.1' \
       --inputHighPtMinbiasHitsFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CampaignInputs/mc21/HITS/mc21_13p6TeV.800831.Py8EG_minbias_inelastic_highjetphotonlepton.merge.HITS.e8453_e8455_s3876_s3880/*' \
       --inputLowPtMinbiasHitsFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CampaignInputs/mc21/HITS/mc21_13p6TeV.900311.Epos_minbias_inelastic_lowjetphoton.merge.HITS.e8453_s3876_s3880/*'  \
       --maxEvents ${NEVENTS} \
       --outputRDOFile 'myRDO.pool.root' \
       --postInclude 'PyJobTransforms.UseFrontier' \
       --preInclude 'HITtoRDO:Campaigns.MC21aSingleBeamspot' \
       --jobNumber '568' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# ttbar digitization MC
run_digi_mc23c(){

    # Parse the inputs
    NEVENTS=${1}

    # Run the job
    export TRF_ECHO=1;
    Digi_tf.py \
       --perfmon 'fullmonmt' \
       --CA 'True'\
       --PileUpPresampling 'True' \
       --conditionsTag 'default:OFLCOND-MC23-SDR-RUN3-01' \
       --digiSeedOffset1 '232' \
       --digiSeedOffset2 '232' \
       --digiSteeringConf 'StandardSignalOnlyTruth' \
       --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' \
       --inputHITSFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/mc23_13p6TeV/HITS/mc23_13p6TeV.900149.PG_single_nu_Pt50.simul.HITS.e8514_e8528_s4153/HITS.33603023._002972.pool.root.1' \
       --inputHighPtMinbiasHitsFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/mc23_13p6TeV/HITS/mc23_13p6TeV.800831.Py8EG_minbias_inelastic_highjetphotonlepton.merge.HITS.e8514_e8528_s4118_s4120/*' \
       --inputLowPtMinbiasHitsFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/mc23_13p6TeV/HITS/mc23_13p6TeV.900311.Epos_minbias_inelastic_lowjetphoton.merge.HITS.e8514_e8528_s4117_s4120/*'  \
       --maxEvents ${NEVENTS} \
       --outputRDOFile 'myRDO.pool.root' \
       --postInclude 'PyJobTransforms.UseFrontier' \
       --preInclude 'HITtoRDO:Campaigns.MC23cSingleBeamspot' \
       --jobNumber '568' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}


# mc21a DAOD
run_mc21a_daod() {

    # Parse the inputs
    FORMAT=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    Derivation_tf.py \
      --CA 'True' \
      --maxEvents ${NEVENTS} \
      --perfmon 'fullmonmt' \
      --inputAODFile '/eos/atlas/atlascerngroupdisk/proj-spot/spot-job-inputs/AODtoDAOD/mc21a/myAOD.pool.root' \
      --outputDAODFile 'pool.root' \
      --formats ${FORMAT} > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}
# phase2 digitization MC
run_phase2_digi(){

    HSHitsFile="/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/PhaseIIUpgrade/HITS/ATLAS-P2-RUN4-03-00-00/mc21_14TeV.900149.PG_single_nu_Pt50.simul.HITS.e8481_s4149/HITS.33990267._000025.pool.root.1"
    HighPtMinbiasHitsFiles="/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/PhaseIIUpgrade/HITS/ATLAS-P2-RUN4-03-00-00/mc21_14TeV.800831.Py8EG_minbias_inelastic_highjetphotonlepton.merge.HITS.e8481_s4149_s4150/*"
    LowPtMinbiasHitsFiles="/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/PhaseIIUpgrade/HITS/ATLAS-P2-RUN4-03-00-00/mc21_14TeV.900311.Epos_minbias_inelastic_lowjetphoton.merge.HITS.e8481_s4149_s4150/*"


    # Parse the inputs
    NEVENTS=${1}

    # Run the job
    export TRF_ECHO=1;
    Digi_tf.py \
       --perfmon 'fullmonmt' \
       --CA 'True'\
       --PileUpPresampling 'True' \
       --conditionsTag 'default:OFLCOND-MC15c-SDR-14-05' \
       --digiSeedOffset1 '170' \
       --digiSeedOffset2 '170' \
       --digiSteeringConf 'StandardSignalOnlyTruth' \
       --geometryVersion 'default:ATLAS-P2-RUN4-03-00-00' \
       --inputHITSFile ${HSHitsFile} \
       --inputHighPtMinbiasHitsFile ${HighPtMinbiasHitsFiles} \
       --inputLowPtMinbiasHitsFile ${LowPtMinbiasHitsFiles}  \
       --maxEvents ${NEVENTS} \
       --outputRDOFile 'myRDO.pool.root' \
       --postInclude 'PyJobTransforms.UseFrontier' \
       --preInclude 'HITtoRDO:Campaigns.PhaseIIPileUp1' \
       --jobNumber '568' > __log.txt 2>&1;

    # Get the exit code
    echo $? > __exitcode;

}

# Define and execute the test
execute() {

    # Define test parameters
    JOBNAME="${1}";
    JOBRELEASE="${2}";
    JOBPLATFORM="${3}";

    echo "${JOBNAME} - ${JOBRELEASE} - ${JOBPLATFORM}"

    # Define the top-level workdir
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs";

    # Create the rundir
    RUNDIR="${WORKDIR}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        mkdir -p ${RUNDIR};
    fi

    # Go to the main rundir
    echo "Using ${RUNDIR} as the rundir...";
    cd "${RUNDIR}";

    # Setup the latest Athena - job runs once per day at a fixed time
    lsetup "asetup Athena,${JOBRELEASE},${JOBPLATFORM//-/,},latest";

    # Check the currently nightly tag
    nightly=`echo "${Athena_DIR##*/${JOBRELEASE}_Athena_${JOBPLATFORM}/}"`;
    nightly=`echo "${nightly%%/Athena/*}"`;

    # Check if it exists already
    if [[ -d "${nightly}" ]]; then
        echo "Directory for ${nightly} already exists, nothing to do."
        return 0;
    fi

    # Now setup the run directory
    mkdir -p "${nightly}"; cd "${nightly}";

    # Let's start
    touch __start;

    # Now run the job :
    # 1) Reco (data18) + DQ w/ N threads over M events
    # 2) Reco (data22) + DQ w/ N threads over M events
    # 3) ITk simulation w/ 16 threads over 200 events
    # 4) q221 based trigger w/ 8 threads over 500 events
    # 5) q445 based full-chain w/ 8 threads over 800 events
    # 6) Derivation production jobs w/ different configurations
    # 7) Phase2 FullSim w/ N threads over M events
    if [[ "${JOBNAME}" == "rawtoall_data18_mt16" ]]; then
        run_r2a_data18 16 2000 1000; # 2000 events in 16 threads
    elif [[ "${JOBNAME}" == "rawtoall_data18_mt8" ]]; then
        run_r2a_data18 8 1000 300; # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "rawtoall_data18_mt1" ]]; then
        run_r2a_data18 1 200 100;  # 200 events in 1 thread
    elif [[ "${JOBNAME}" == "rawtoall_data22_mt16" ]]; then
        run_r2a_data22 16 2000 1000; # 2000 events in 16 threads
    elif [[ "${JOBNAME}" == "rawtoall_data22_mt8" ]]; then
        run_r2a_data22 8 1000 300; # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "rawtoall_data22_mt1" ]]; then
        run_r2a_data22 1 200 100;  # 200 events in 1 thread
    elif [[ "${JOBNAME}" == "rawtoall_data23_mt8" ]]; then
        run_r2a_data23 8 1000 300; # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "rawtoall_data23_mt1" ]]; then
        run_r2a_data23 1 200 100;  # 200 events in 1 thread
    elif [[ "${JOBNAME}" == "rawtoall_r3_data23_mt8" ]]; then
        run_r3a_data23 8 1000 300; # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "rawtoall_r3_data23_mt1" ]]; then
        run_r3a_data23 1 200 100; # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "evnttohits_itk_ca_mt16" ]]; then
        run_e2h_itk 16 250;  # 250 events in 16 threads
    elif [[ "${JOBNAME}" == "rdotordotrigger_q221_mt8" ]]; then
        run_r2rt 8 500;  # 500 events in 8 threads
    elif [[ "${JOBNAME}" == "fullchain_q445_mt8" ]]; then
        run_q445 8 800;  # 800 events in 8 threads
    elif [[ "${JOBNAME}" == "fullchain_mc21a_mt8" ]]; then
        run_mc21a 8 1000;  # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "fullchain_mc23c_mt8" ]]; then
        run_mc23c 8 1000;  # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "data22_daod_phys" ]]; then
        run_data22_daod "PHYS" 1000;  # 1000 events PHYS w/ data22
    elif [[ "${JOBNAME}" == "data22_daod_physlite" ]]; then
        run_data22_daod "PHYSLITE" 1000;  # 1000 events PHYSLITE w/ data22
    elif [[ "${JOBNAME}" == "data23_daod_phys" ]]; then
        run_data23_daod "PHYS" 1000;  # 1000 events PHYS w/ data22
    elif [[ "${JOBNAME}" == "data23_daod_physlite" ]]; then
        run_data23_daod  "PHYSLITE" 1000;  # 1000 events PHYSLITE w/ data22
    elif [[ "${JOBNAME}" == "data23_daod_phys_ntp" ]]; then
        run_data23_daod_ntp "PHYS" 1000;  # 1000 events PHYS w/ data22
    elif [[ "${JOBNAME}" == "data23_daod_physlite_ntp" ]]; then
        run_data23_daod_ntp "PHYSLITE" 1000;  # 1000 events PHYSLITE w/ data22
    elif [[ "${JOBNAME}" == "mc21a_daod_phys" ]]; then
        run_mc21a_daod "PHYS" 1000;  # 1000 events PHYS w/ mc21a
    elif [[ "${JOBNAME}" == "mc21a_daod_physlite" ]]; then
        run_mc21a_daod "PHYSLITE" 1000;  # 1000 events PHYSLITE w/ mc21a
    elif [[ "${JOBNAME}" == "mc21a_daod_phys_ntp" ]]; then
        run_mc21a_daod_ntp "PHYS" 1000;  # 1000 events PHYS w/ mc21a
    elif [[ "${JOBNAME}" == "mc21a_daod_physlite_ntp" ]]; then
        run_mc21a_daod_ntp "PHYSLITE" 1000;  # 1000 events PHYSLITE w/ mc21a
    elif [[ "${JOBNAME}" == "phase2_fullsim_mt8" ]]; then
        run_phase2_fullsim_mt8 8 200; # 200 events in 8 thread
    elif [[ "${JOBNAME}" == "phase2_recoonly_fasttracking" ]]; then
        run_phase2_recoonly_fasttracking 1 100; # 100 events in 1 thread
    elif [[ "${JOBNAME}" == "phase2_recoonly_tracking" ]]; then
         run_phase2_recoonly_tracking 1 100;
    elif [[ "${JOBNAME}" == "phase2_recoonly_actstracking" ]]; then
         $Athena_DIR/src/Tracking/Acts/ActsConfig/test/ActsBenchmarkWithSpot.sh 1 100;
    elif [[ "${JOBNAME}" == "phase2_recoonly_actstracking_cached" ]]; then
         $Athena_DIR/src/Tracking/Acts/ActsConfig/test/ActsBenchmarkWithSpotCached.sh 1 100;
    elif [[ "${JOBNAME}" == "digi_serial_phase2" ]]; then
         run_phase2_digi 50;
    elif [[ "${JOBNAME}" == "ttbar_fullsim_mt8" ]]; then
         run_mcttbar 8 100;
    elif [[ "${JOBNAME}" == "ttbar_fastsim_mt8" ]]; then
         run_mcttbar_fast 8 400;
    elif [[ "${JOBNAME}" == "ttbar_digi_serial" ]]; then
         run_digi_ttbar 50;
    elif [[ "${JOBNAME}" == "digi_serial_mc23c" ]]; then
         run_digi_mc23c 50;
    elif [[ "${JOBNAME}" == "hp_recoonly_mt8" ]]; then
         run_hi_hp 8 500;
    elif [[ "${JOBNAME}" == "upc_recoonly_mt8" ]]; then
         run_hi_upc 8 1000;
    else
        echo "Unknown job ${JOBNAME}, quitting..."
        return 0
    fi

    # Now check the POOL content
    if [[ -f "myHITS.pool.root" ]]; then
        checkxAOD.py myHITS.pool.root > myHITS.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "myRDO.pool.root" ]]; then
        checkxAOD.py myRDO.pool.root > myRDO.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "myRDOTrigger.pool.root" ]]; then
        checkxAOD.py myRDOTrigger.pool.root > myRDOTrigger.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "myESD.pool.root" ]]; then
        checkxAOD.py myESD.pool.root > myESD.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "myAOD.pool.root" ]]; then
        checkxAOD.py myAOD.pool.root > myAOD.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "DAOD_PHYS.pool.root" ]]; then
        checkxAOD.py DAOD_PHYS.pool.root > myDAOD.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "DAOD_PHYSLITE.pool.root" ]]; then
        checkxAOD.py DAOD_PHYSLITE.pool.root > myDAOD.pool.root.checkfile.txt 2>/dev/null;
    fi

    # Cleanup the POOL files to save disk-space
    rm -f *.pool.root;
    if [[ "${JOBNAME}" == "fullchain_mc21a_mt8" ]]; then
        rm -f tmp.RDO;
        rm -f tmp.RDO_TRIG;
    fi
    if [[ "${JOBNAME}" == "fullchain_mc23c_mt8" ]]; then
        rm -f tmp.RDO;
        rm -f tmp.RDO_TRIG;
    fi
    # Let's extract the transform command to be used on the webpage
    echo "#!/bin/bash" > __command.txt;
    if [[ -f "env.txt" ]]; then
        echo "export $( grep "ATHENA_CORE_NUMBER" env.txt )" >> __command.txt;
    fi
python3 << END
import json
with open("__command.txt","a") as outfile, open("jobReport.json") as infile:
    data = json.load(infile) # Load the job report
    cmd = data['cmdLine'].split("' '") # Extract the transform command
    tf  = cmd[0].split('/')[-1] # This is the main transform, strip away full path
    cmd = [ f"{val} " if "--" in val else f"'{val}' " for val in cmd[1:] ]
    cmd = [ val.replace("/data/atlaspmb/","/eos/atlas/atlascerngroupdisk/proj-spot/") for val in cmd ]
    cmd = [ val.replace("--","\\\\\n  --") for val in cmd ]
    cmd = [ val.replace("''","'") for val in cmd ]
    outfile.write(f"{tf} {''.join(cmd)}")
END

    # Let's archive the results on EOS
    nightlydate=$( echo "${nightly}" | cut -c1-10 );
    tokens=( $( echo "${nightlydate}" | tr "-" " " ) );
    YEAR="${tokens[0]}";
    MONTH="${tokens[1]}";
    DAY="${tokens[2]}";
    TARGET_DIR="${ARCHIVE_DIR}/${DAY}/${MONTH}/${YEAR}/${JOBRELEASE}/${JOBPLATFORM}/spot-mon-${JOBNAME}";
    echo "Copying results into ${TARGET_DIR}";
    mkdir -p ${TARGET_DIR};
    rsync -avuz __* prmon* log* perfmonmt* *checkfile* runargs* runwrapper* env.txt ${TARGET_DIR}/.;

    # Copy the command to the webpage area
    TARGET_DIR="${WEB_DIR}/spot-mon-${JOBNAME}/pages/commands";
    echo "Copying commands into ${TARGET_DIR}";
    mkdir -p ${TARGET_DIR};
    rsync -avuz __command.txt ${TARGET_DIR}/${DAY}-${MONTH}-${YEAR}-${JOBRELEASE}-${JOBPLATFORM}-${JOBNAME}

    # Copy the logs to the webpage area
    TARGET_DIR="${WEB_DIR}/spot-mon-${JOBNAME}/pages/logs";
    echo "Copying logs into ${TARGET_DIR}";
    mkdir -p ${TARGET_DIR};
    for file in $( ls log.* )
    do
        tokens=( $( echo "${file}" | tr "." " "  ) );
        JOBSTEP="${tokens[1]}"
        if [[ "${JOBSTEP}" == "DQHistogramMerge" ]]; then
            continue
        fi
        rsync -avuz ${file} ${TARGET_DIR}/${DAY}-${MONTH}-${YEAR}-${JOBRELEASE}-${JOBPLATFORM}-${JOBNAME}-${JOBSTEP}
    done

    # All done
    touch __done;

    # Go back to rundir
    cd "${RUNDIR}";

}

# Define the main function
main() {

    # Setup environment
    source ~/.bashrc;
    source ~/.bash_profile;
    eosfusebind -g # ATLINFR-3256

    # Define the jobs this setup will attempt to run
    if [[ -z "${2}" ]]; then
        # For the time being make sure only a single instance of this script is running
        # This prevents jobs running on top of each other by serializing the executions
        # In the future we can changes this logic...
        pidof -o %PPID -x $0 >/dev/null && echo "WARNING: Script ${0} already running, nothing to do..." && exit 0

        # These are the standard jobs
        JOBS=( "23.0 x86_64-centos7-gcc11-opt rawtoall_data23_mt8" \
               "23.0 x86_64-centos7-gcc11-opt fullchain_q445_mt8" \
               "23.0 x86_64-centos7-gcc11-opt rawtoall_data23_mt1" \
               "23.0 x86_64-centos7-gcc11-opt fullchain_mc23c_mt8" \
               "23.0 x86_64-centos7-gcc11-opt rawtoall_r3_data23_mt8" \
               "23.0 x86_64-centos7-gcc11-opt rawtoall_r3_data23_mt1" \
               "main x86_64-centos7-gcc11-opt rawtoall_data23_mt8" \
               "main x86_64-centos7-gcc11-opt rawtoall_r3_data23_mt8" \
               "main x86_64-centos7-gcc11-opt fullchain_mc23c_mt8" \
               "main x86_64-centos7-gcc11-opt fullchain_q445_mt8" \
               "main x86_64-centos7-gcc11-opt rawtoall_data23_mt1" \
               "main x86_64-centos7-gcc11-opt rawtoall_r3_data23_mt1" \
               "main x86_64-centos7-clang16-opt fullchain_q445_mt8" \
               "main x86_64-centos7-clang16-opt rawtoall_r3_data23_mt8" \
               "main x86_64-centos7-clang16-opt fullchain_mc23c_mt8" \
               "main x86_64-centos7-gcc11-opt ttbar_fullsim_mt8" \
               "main x86_64-centos7-gcc11-opt ttbar_fastsim_mt8" \
               "main x86_64-centos7-gcc11-opt digi_serial_mc23c" \
               "23.0 x86_64-centos7-gcc11-opt digi_serial_mc23c" \
               "23.0 x86_64-centos7-gcc11-opt ttbar_fullsim_mt8" \
               "23.0 x86_64-centos7-gcc11-opt ttbar_fastsim_mt8" \
               "main--archflagtest x86_64-centos7-gcc11-opt rawtoall_r3_data23_mt8" \
               "main--archflagtest x86_64-centos7-gcc11-opt fullchain_mc23c_mt8" \
               "main--ltoflagtest  x86_64-centos7-gcc11-opt rawtoall_r3_data23_mt8" \
               "main--ltoflagtest x86_64-centos7-gcc11-opt fullchain_mc23c_mt8" \
               "main--HepMC2 x86_64-centos7-gcc11-opt fullchain_mc23c_mt8" \
               "main x86_64-centos7-gcc11-opt hp_recoonly_mt8" \
               "main x86_64-centos7-gcc11-opt upc_recoonly_mt8" \
               "23.0 x86_64-centos7-gcc11-opt hp_recoonly_mt8" \
               "23.0 x86_64-centos7-gcc11-opt upc_recoonly_mt8")
    elif [[ "${2}" == "derivation" ]]; then
        # These are the derivation jobs
        JOBS=( "main x86_64-centos7-gcc11-opt data22_daod_phys" \
               "main x86_64-centos7-gcc11-opt mc21a_daod_phys" \
               "main x86_64-centos7-gcc11-opt data23_daod_phys" \
               "main x86_64-centos7-gcc11-opt data22_daod_physlite" \
               "main x86_64-centos7-gcc11-opt mc21a_daod_physlite" \
               "main x86_64-centos7-gcc11-opt data23_daod_physlite")
    elif [[ "${2}" == "phase2" ]]; then
        # These are the phase2 jobs
        JOBS=( "main x86_64-centos7-gcc11-opt phase2_fullsim_mt8" \
               "main x86_64-centos7-gcc11-opt phase2_recoonly_fasttracking" \
               "main x86_64-centos7-gcc11-opt phase2_recoonly_tracking" \
               "main x86_64-centos7-gcc11-opt phase2_recoonly_actstracking" \
               "main x86_64-centos7-gcc11-opt digi_serial_phase2")
    elif [[ "${2}" == "el9_test" ]]; then
        # These are jobs to test the new EL9 OS
        JOBS=("24.0 x86_64-el9-gcc13-opt rawtoall_data23_mt8" \
               "24.0 x86_64-el9-gcc13-opt fullchain_q445_mt8" \
               "24.0 x86_64-el9-gcc13-opt rawtoall_data23_mt1" \
               "24.0 x86_64-el9-gcc13-opt fullchain_mc23c_mt8" \
               "24.0 x86_64-el9-gcc13-opt rawtoall_r3_data23_mt8" \
               "24.0 x86_64-el9-gcc13-opt rawtoall_r3_data23_mt1" \
               "24.0 x86_64-el9-gcc13-opt digi_serial_mc23c" \
               "24.0 x86_64-el9-gcc13-opt ttbar_fullsim_mt8" \
               "24.0 x86_64-el9-gcc13-opt ttbar_fastsim_mt8" \
               "24.0 x86_64-el9-gcc13-opt hp_recoonly_mt8" \
               "24.0 x86_64-el9-gcc13-opt upc_recoonly_mt8" \
               "main x86_64-el9-gcc13-opt rawtoall_data23_mt8" \
               "main x86_64-el9-gcc13-opt rawtoall_r3_data23_mt8" \
               "main x86_64-el9-gcc13-opt fullchain_mc23c_mt8" \
               "main x86_64-el9-gcc13-opt fullchain_q445_mt8" \
               "main x86_64-el9-gcc13-opt rawtoall_data23_mt1" \
               "main x86_64-el9-gcc13-opt rawtoall_r3_data23_mt1" \
               "main--HepMC2 x86_64-el9-gcc13-opt fullchain_mc23c_mt8" \
               "main x86_64-el9-gcc13-opt ttbar_fullsim_mt8" \
               "main x86_64-el9-gcc13-opt ttbar_fastsim_mt8" \
               "main x86_64-el9-gcc13-opt digi_serial_mc23c" \
               "main--HepMC2 x86_64-el9-gcc13-opt digi_serial_mc23c" \
               "main x86_64-el9-gcc13-opt hp_recoonly_mt8" \
               "main x86_64-el9-gcc13-opt upc_recoonly_mt8")
    elif [[ "${2}" == "el9_derivation" ]]; then
        # These are the derivation jobs to test the new EL9 OS
        JOBS=( "main x86_64-el9-gcc13-opt data22_daod_phys" \
               "main x86_64-el9-gcc13-opt mc21a_daod_phys" \
               "main x86_64-el9-gcc13-opt data23_daod_phys" \
               "main x86_64-el9-gcc13-opt data22_daod_physlite" \
               "main x86_64-el9-gcc13-opt mc21a_daod_physlite" \
               "main x86_64-el9-gcc13-opt data23_daod_physlite" \
               "main--dev4LCG x86_64-el9-gcc13-opt data23_daod_physlite_ntp" \
               "main--dev4LCG x86_64-el9-gcc13-opt data23_daod_phys_ntp" \
               "main--dev4LCG x86_64-el9-gcc13-opt mc21a_daod_physlite_ntp" \
               "main--dev4LCG x86_64-el9-gcc13-opt mc21a_daod_phys_ntp")
    elif [[ "${2}" == "el9_phase2" ]]; then
        # These are the phase2 jobs to test the new EL9 OS
        JOBS=( "main x86_64-el9-gcc13-opt phase2_fullsim_mt8" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_fasttracking" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_tracking" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_actstracking" \
               "main x86_64-el9-gcc13-opt phase2_recoonly_actstracking_cached" \
               "main x86_64-el9-gcc13-opt digi_serial_phase2")
    else
        JOBS=()
    fi

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        # Match the release w/ the user input
        if [[ "${1}" == "${jobrelease}" ]]; then
            execute "${jobname}" "${jobrelease}" "${jobplatform}"
        fi
    done

}

# Execute the main function
main "${1}" "${2}"
