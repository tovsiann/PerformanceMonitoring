#!/usr/bin/env python3

__author__ = "Alaettin Serhan Mete <amete@cern.ch>"
__version__ = "0.0.1"

import SPOTDatabase as sdb
import argparse
import glob
import os

def create_file(indir, outdir, jobname, jobrelease, jobplatform):
    """ Main execution function """

    folders = glob.glob(f"{indir}/*T*")

    for folder in folders:

        # Make sure we're done first...
        nightly = folder.split('/')[-1]
        infile=f"{indir}/{nightly}/__done"

        if not os.path.isfile(infile):
            print(f"Job ({jobname}) for release/platform ({jobrelease}/{jobplatform}) doesn't seem to have finished...")
            continue
        # Create the DB file
        myobj = sdb.SPOTDatabase(f"{outdir}/{os.path.dirname(os.path.realpath(infile)).replace('/','__')}.db")

        # Add to the DB
        myobj.addEntry(jobrelease,jobplatform,nightly,jobname,f"{indir}/{nightly}")

def main():
    """ Main function """

    # Parse the user inputs
    parser = argparse.ArgumentParser(description = 'Script to create database files for the daily SPOT monitoring job')

    parser.add_argument('-i', '--indir', type = str, required = True,
                        help = 'Input directory where the tests were run')
    parser.add_argument('-o', '--outdir', type = str, required = True,
                        help = 'Output directory for the database files')
    parser.add_argument('-j', '--jobname', type = str, default = 'rawtoall_data18_mt8',
                        help = 'Name of the job for which to create the plots')
    parser.add_argument('-r', '--release', type = str, default = 'master',
                        help = 'Name of the release for which to create the plots ')
    parser.add_argument('-p', '--platform', type = str, default = 'x86_64-centos7-gcc8-opt',
                        help = 'Name of the platform for which to create the plots ')

    args = parser.parse_args()

    create_file(indir        = args.indir,
                outdir       = args.outdir,
                jobname      = args.jobname,
                jobrelease   = args.release,
                jobplatform  = args.platform)

if '__main__' in __name__:
    main()
