From time to time PMB is asked to produce public plots that show some measure of technical performance as a function of software release. See

https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/ComputingAndSoftwareForApproval#Software_Performance_Plots

for examples of plots that have been made in the past. 


To achieve this a C++ program is used that relies on ROOT libraries and Atlas Plot style code and guidelines. The latter are described in

https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComPlotStyle

The file used to make plots had been place in the src directory of this repository. As mentioned in depends on the atlasstyle package, which can be retrieved at the above twiki page 

https://gitlab.cern.ch/atlaspmb/PerformanceMonitoring/tree/master/src

In principle typing
'''
make
'''

in the PerformanceMonitoring/src directory should be enough to build the executable that generates the plots