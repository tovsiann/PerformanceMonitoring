#!/bin/bash
# Define and execute the main job
execute() {

    # Define test parameters
    JOBNAME="${1}";
    JOBRELEASE="${2}";
    JOBPLATFORM="${3}";

    BASEDIR="/afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring";
    lsetup "views LCG_104b ${JOBPLATFORM}"
    export PYTHONPATH=$BASEDIR/python:$PYTHONPATH;

    # Define the top-level workdir
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs/valgrind/memcheck";

    # Create the rundir
    RUNDIR="${WORKDIR}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        echo "Directory ${RUNDIR} doesn't exist, nothing to do...";
        return 0;
    fi
    cd ${RUNDIR};
    if [[ ! -d "${HOME}/www_atlaspmb/athenamt-${JOBNAME}-valgrind-reports" ]]; then

          mkdir -p ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-valgrind-reports;

    fi
    nightles_=($(find . -name "*T*" -type d -printf "%f\n"));
    i=0;
    declare -a  nightles;
    for nightly in "${nightles_[@]}"
    do
     if [[ ! -d "${HOME}/www_atlaspmb/athenamt-${JOBNAME}-valgrind-reports/${nightly}"  ]]; then
        nightles[$i]="${nightly}"
        i=$((i+1));
     fi
    done

    cd ${RUNDIR};
      for nightly in "${nightles[@]}"
      do
        cd "${nightly}";

        mkdir -p ${HOME}/www_atlaspmb/athenamt-${JOBNAME}-valgrind-reports/${nightly};
        python ${BASEDIR}/valgrind/analyze-memcheck-logs.py -i valgrind.*.1.out -l > "${HOME}/www_atlaspmb/athenamt-${JOBNAME}-valgrind-reports/${nightly}/${nightly}.${JOBNAME}-10evts.loop.txt"
        cd ..;       
     done
     cd ~/;  
}


# Define the main function
main() {

    # Setup environment

    source ~/.bashrc;
    source ~/.bash_profile;

    # Define the jobs this setup will attempt to run
    JOBS=( "main x86_64-el9-gcc13-opt rawtoall_data23_mt8" \
           "main x86_64-el9-gcc13-opt ttbar_fullsim_mt8" )

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        # Process the job
        execute "${jobname}" "${jobrelease}" "${jobplatform}"
    done

}

# Execute main
main
